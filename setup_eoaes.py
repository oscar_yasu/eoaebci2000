#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 12:54:51 2019

@author: oscar
"""

# Acceso al sistema de archivos
import os
# Carga las funciones de construcción de la GUI
from setup_eoaes_ui import *
# Carga las utilerías para BCI2000
from bci2000_eoae_utils import *

# Clase de la ventana principal, con herencia múltiple
# de QMainWindow y de Ui_MainWindow
class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    # Constructor
    def __init__(self, *args, **kwargs):
        # Constructor de QMainWindow
        QtWidgets.QMainWindow.__init__(self, *args, **kwargs)
        # Elaborador de la GUI
        self.setupUi(self)
        # Callbacks
        self.salirPushButton.clicked.connect(self.salir)
        self.confirmarPushButton.clicked.connect(self.crear_sujeto)
        self.nombreLineEdit.textChanged.connect(self.actualiza_id)
        
    # Termina la aplicación    
    def salir(self):
        QtWidgets.QApplication.exit()
    
    # Crea directorio y archivos del sujeto    
    def crear_sujeto(self):
        # Crea nombre del directorio
        directorio = '{}{}'.format(EOAEDIR,self.idoutLabel.text())
        if not os.path.exists(directorio):
            # Crea directorio
            os.mkdir(directorio)
            # Almacena nombre del sujeto
            bci2000_eoae_nombre(directorio,self.nombreLineEdit.text())
            # Almacena parametros personalizados
            bci2000_eoae_sequence(directorio,self.idoutLabel.text())
            # Almacena operator script personalizado
            bci2000_eoae_batch(directorio,self.idoutLabel.text())
        else:
            QtWidgets.QMessageBox.warning(self,'Atención','El directorio ya existe')
            
    # Procesa el nombre y crea el id        
    def actualiza_id(self,texto):
        # Separa texto en palabras
        palabras = texto.split()
        # Recupera iniciales por palabra
        iniciales = [palabra[0] for palabra in palabras]
        # Genera id en mayúsculas
        self.idoutLabel.setText(''.join(iniciales).upper())
                    
# Programa principal
if __name__ == "__main__":
    # Crea aplicación
    app = QtWidgets.QApplication([])
    # Crea y muestra ventana de la aplicación
    window = MainWindow()
    window.show()
    # Arranca aplicación
    app.exec_()