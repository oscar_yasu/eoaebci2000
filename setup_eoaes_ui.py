# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'setup_eoaes.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(615, 130)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.nombreLabel = QtWidgets.QLabel(self.centralwidget)
        self.nombreLabel.setObjectName("nombreLabel")
        self.horizontalLayout.addWidget(self.nombreLabel)
        self.nombreLineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.nombreLineEdit.setObjectName("nombreLineEdit")
        self.horizontalLayout.addWidget(self.nombreLineEdit)
        self.idLabel = QtWidgets.QLabel(self.centralwidget)
        self.idLabel.setObjectName("idLabel")
        self.horizontalLayout.addWidget(self.idLabel)
        self.idoutLabel = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.idoutLabel.sizePolicy().hasHeightForWidth())
        self.idoutLabel.setSizePolicy(sizePolicy)
        self.idoutLabel.setObjectName("idoutLabel")
        self.horizontalLayout.addWidget(self.idoutLabel)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.confirmarPushButton = QtWidgets.QPushButton(self.centralwidget)
        self.confirmarPushButton.setObjectName("confirmarPushButton")
        self.verticalLayout.addWidget(self.confirmarPushButton)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.salirPushButton = QtWidgets.QPushButton(self.centralwidget)
        self.salirPushButton.setObjectName("salirPushButton")
        self.horizontalLayout_2.addWidget(self.salirPushButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Paradigma EOAEs"))
        self.nombreLabel.setText(_translate("MainWindow", "Nombre:"))
        self.nombreLineEdit.setText(_translate("MainWindow", "PRUEBA"))
        self.idLabel.setText(_translate("MainWindow", "ID:"))
        self.idoutLabel.setText(_translate("MainWindow", "PRUEBA"))
        self.confirmarPushButton.setText(_translate("MainWindow", "Confirmar"))
        self.salirPushButton.setText(_translate("MainWindow", "Salir"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

