#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 14:26:49 2019

@author: oscar
"""
import os
import random

BCI2KDIR = 'C:\\PROGRA~1\\BCI2000_v3\\BCI2000\\prog\\'
EOAEDIR = 'C:\\eoae_datos\\'
#EOAEDIR = '/home/oscar/Proyectos/EOAEs/eoae_bci2k/eoae_datos/'
PROTOCOLDIR = 'C:\\eoaebci2000\\'
#PROTOCOLDIR = '/home/oscar/Proyectos/EOAEs/eoae_bci2k/'
BASEPARAMFILE = PROTOCOLDIR + 'eoae_base_parameters.prm'
STIMULUSMATRIXFILE = PROTOCOLDIR + 'eoae_base_stimulus_matrix.bmt'

B2KSOURCE = 'SignalGenerator.exe 127.0.0.1'
B2KPROCESSING = 'DummySignalProcessing.exe 127.0.0.1'
B2KAPP = 'StimulusPresentation.exe 127.0.0.1'
        
# Almacena nombre en archivo de texto
def bci2000_eoae_nombre(directorio, nombre):
    file = open(directorio + '/nombre.txt','wb')
    file.write(nombre.encode('utf8'))
    file.close()
    
# Crea secuencia de estímulos
def bci2000_eoae_sequence(directorio,idstr):
    # Genera lista de estímulos
    defs,estimulos = bci2000_eoae_estimulos()
    # Estímulos previos a secuencia de audio
    preaudio = [estimulos.index(est)+1 for est in ['audio_cue', 'expectation']]
    # Estímulos previos a secuencia de audio
    prevideo = [estimulos.index(est)+1 for est in ['video_cue', 'expectation']]
    # Estímulos de secuencia de imaginación
    imagina = [estimulos.index(est)+1 for est in ['imagery_countdown_blank', 'imagery_countdown_03', 'imagery_countdown_02', 'imagery_countdown_01', 'imagery_countdown_00', 'imagery_countdown_blank', 'imagery', 'rest']]
    # Lista de los ocho distintos frames de estimulación
    frames = list()
    frames.append(preaudio + [estimulos.index('audio_simple_01')+1] + imagina)
    frames.append(preaudio + [estimulos.index('audio_simple_02')+1] + imagina)
    frames.append(preaudio + [estimulos.index('audio_complex_01')+1] + imagina)
    frames.append(preaudio + [estimulos.index('audio_complex_02')+1] + imagina)
    frames.append(prevideo + [estimulos.index('video_simple_011')+1, estimulos.index('video_simple_012')+1] + imagina)
    frames.append(prevideo + [estimulos.index('video_simple_021')+1, estimulos.index('video_simple_022')+1] + imagina)
    frames.append(prevideo + [i+1 for i,est in enumerate(estimulos) if 'video_complex_01' in est] + imagina)
    frames.append(prevideo + [i+1 for i,est in enumerate(estimulos) if 'video_complex_02' in est] + imagina)
    # Genera orden aleatorio de tres repeticiones de frames
    orden = list()
    for rep in range(3):
        orden += random.sample(range(8),8)
    # Escribe archivo de parámetros
    if os.name == 'posix':
        file = open(directorio + '/{}_sequence.prm'.format(idstr),'wt')
    else:
        file = open(directorio + '\\{}_sequence.prm'.format(idstr),'wt')
    # cadena con directorio del sujeto
    dir_str = 'Storage:Data%20Location:DataIOFilter string DataDirectory= {} % % // path to top level data directory (directory)\n'.format(directorio)
    file.write(dir_str)    
    # Cadena con id de sujeto para archivos
    subject_str = 'Storage:Session:DataIOFilter string SubjectName= {} Name % % // subject alias\n'.format(idstr)
    file.write(subject_str)
    # Cadena de secuencia de estímulos
    secuencia = [estimulos.index('baseline') + 1]
    for inx in orden:
        secuencia += frames[inx]
    secuencia.insert(0,len(secuencia))    
    seq_str = ' '.join([str(i) for i in secuencia])
    seq_str = 'Application:Sequencing:StimulusPresentationTask intlist Sequence= {} // Sequence in which stimuli are presented (deterministic mode)/ Stimulus frequencies for each stimulus (random mode)/ Ignored (P3Speller compatible mode)\n'.format(seq_str)
    file.write(seq_str)
    file.close()
    # Agrega al archivo de nombre la secuencia de frames
    frame_names = ['as1','as2','ac1','ac2','vs1','vs2','vc1','vc2']
    if os.name == 'posix':
        file = open(directorio + '/nombre.txt','a')
    else:
        file = open(directorio + '\\nombre.txt','a')
    for i in orden:
        file.write('\n{} '.format(frame_names[i]))
    file.close()
    
# Crea el operator script para el usuario
def bci2000_eoae_batch(directorio,idstr):
    if os.name == 'posix':
        SEQUENCEPARAMFILE = directorio + '/{}_sequence.prm'.format(idstr)
    else:
        SEQUENCEPARAMFILE = directorio + '\\{}_sequence.prm'.format(idstr)

    ONCONNECT = '-LOAD PARAMETERFILE {0:s}; '.format(BASEPARAMFILE)
    ONCONNECT += 'LOAD PARAMETERFILE {0:s}; '.format(STIMULUSMATRIXFILE)
    ONCONNECT += 'LOAD PARAMETERFILE {0:s}; '.format(SEQUENCEPARAMFILE)
    ONCONNECT += 'SETCONFIG;'

    if os.name == 'posix':
        file = open(directorio + '/{}_bci2000.bat'.format(idstr),'wt')
    else:
        file = open(directorio + '\\{}_bci2000.bat'.format(idstr),'wt')
    file.write('cd {0:s}\n'.format(directorio))
    file.write('start {0:s}operator.exe ^\n'.format(BCI2KDIR))
    file.write('   --Title %~n0 ^\n')
    file.write('   --OnConnect "{}"\n'.format(ONCONNECT))
    file.write('start {0:s}{1:s}\n'.format(BCI2KDIR,B2KSOURCE))
    file.write('start {0:s}{1:s}\n'.format(BCI2KDIR,B2KPROCESSING))
    file.write('start {0:s}{1:s}\n'.format(BCI2KDIR,B2KAPP))
    file.close()

# Genera la matriz de estímulos    
def bci2000_eoae_estimulos():
    # nombre: (caption, image, audio, duratestimion)
    estdir = dict()
    estlst = list()
    estdir['baseline'] = ('+', '%', '%', '60s')
    estlst.append('baseline')
    estdir['audio_cue'] = ('%', PROTOCOLDIR + 'audio/audiocue.bmp', '%', '3s')
    estlst.append('audio_cue')
    estdir['video_cue'] = ('%', PROTOCOLDIR + 'video/videocue.bmp', '%', '3s')
    estlst.append('video_cue')
    estdir['expectation'] = ('%', '%', '%', '8s')
    estlst.append('expectation')
    estdir['audio_simple_01'] = ('%', '%', PROTOCOLDIR + 'audio/audio_simple_01.wav', '8s')
    estlst.append('audio_simple_01')
    estdir['audio_simple_02'] = ('%', '%', PROTOCOLDIR + 'audio/audio_simple_02.wav', '8s')
    estlst.append('audio_simple_02')
    estdir['audio_complex_01'] = ('%', '%', PROTOCOLDIR + 'audio/audio_complex_01.wav', '8s')
    estlst.append('audio_complex_01')
    estdir['audio_complex_02'] = ('%', '%', PROTOCOLDIR + 'audio/audio_complex_02.wav', '8s')
    estlst.append('audio_complex_02')
    estdir['imagery_countdown_blank'] = ('%', '%', '%', '1s')
    estlst.append('imagery_countdown_blank')
    estdir['imagery_countdown_03'] = ('3...', '%', '%', '1s')
    estlst.append('imagery_countdown_03')
    estdir['imagery_countdown_02'] = ('2...', '%', '%', '1s')
    estlst.append('imagery_countdown_02')
    estdir['imagery_countdown_01'] = ('1...', '%', '%', '1s')
    estlst.append('imagery_countdown_01')
    estdir['imagery_countdown_00'] = ('%', '%', PROTOCOLDIR + 'audio/audio_beep.wav', '1s')
    estlst.append('imagery_countdown_00')
    estdir['imagery'] = ('+', '%', '%', '8s')
    estlst.append('imagery')
    estdir['rest'] = ('descansa', '%', '%', '10s')
    estlst.append('rest')
    estdir['video_simple_011'] = ('%', PROTOCOLDIR + 'video/video_simple_011.bmp', '%', '4s')
    estlst.append('video_simple_011')
    estdir['video_simple_012'] = ('%', PROTOCOLDIR + 'video/video_simple_012.bmp', '%', '4s')    
    estlst.append('video_simple_012')
    estdir['video_simple_021'] = ('%', PROTOCOLDIR + 'video/video_simple_021.bmp', '%', '4s')
    estlst.append('video_simple_021')
    estdir['video_simple_022'] = ('%', PROTOCOLDIR + 'video/video_simple_022.bmp', '%', '4s')
    estlst.append('video_simple_022')
    for i in range(len(os.listdir(PROTOCOLDIR + 'video/boing'))):
        estdir['video_complex_01_{0:03d}'.format(i)] = ('%', PROTOCOLDIR + 'video/boing/boing{0:04d}.bmp'.format(i), '%', '31.25ms')
        estlst.append('video_complex_01_{0:03d}'.format(i))
    for i in range(len(os.listdir(PROTOCOLDIR + 'video/pvilla'))):
        estdir['video_complex_02_{0:03d}'.format(i)] = ('%', PROTOCOLDIR + 'video/pvilla/pvilla{0:03d}.bmp'.format(i), '%', '31.25ms')
        estlst.append('video_complex_02_{0:03d}'.format(i))
    return estdir, estlst

# Crea el archivo de stimulus matrix
def bci2000_eoae_stimulus_matrix():
    # Genera los estimulos
    estimulos, estlista = bci2000_eoae_estimulos()
    # Crea encabezado y pie del archivo .bmt
    header = 'Application:Stimuli:StimulusPresentationTask matrix Stimuli= { caption icon audio StimulusDuration }'
    footer = '// captions and icons to be displayed, sounds to be played for different stimuli'
    # Inicializa las cadenas de definición
    keystr = ''
    captionstr = ''
    iconstr = ''
    audiostr = ''
    durationstr = ''
    # Llena las cadenas
    for key in estlista:
        keystr += key + ' '
        captionstr += estimulos[key][0] + ' '
        iconstr += estimulos[key][1] + ' '
        audiostr += estimulos[key][2] + ' '
        durationstr += estimulos[key][3] + ' '
    # Genera el archivo
    sm = header + ' { '+ keystr + '} ' + captionstr + iconstr + audiostr + durationstr + ' ' + footer
    #sm.replace('/','\\')           
    file = open(STIMULUSMATRIXFILE,'wt')
    file.write(sm)
    file.close()
    
